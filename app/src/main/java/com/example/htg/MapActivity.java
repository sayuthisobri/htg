package com.example.htg;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MapActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_map);
    WebView webView = findViewById(R.id.webView);
    webView.getSettings().setJavaScriptEnabled(true);
    String coordinate = getIntent().getStringExtra("coordinates");
    if (coordinate != null) {
      webView.loadUrl("https://www.google.com/maps/search/?api=1&query=" + coordinate);
    } else {
      Toast.makeText(this, "Opps! No coordinate found for this place", Toast.LENGTH_SHORT).show();
      onBackPressed();
    }
  }
}