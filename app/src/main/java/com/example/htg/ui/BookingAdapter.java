package com.example.htg.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.htg.R;
import com.example.htg.models.BookingData;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.BookingViewHolder> {
  private final Context context;
  private final List<BookingData> bookings;

  public BookingAdapter(Context context, List<BookingData> bookings) {
    this.context = context;
    this.bookings = bookings;
  }

  public void clear() {
    this.bookings.clear();
  }

  public void add(BookingData data) {
    this.bookings.add(data);
  }

  @NonNull
  @NotNull
  @Override
  public BookingViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    View view = layoutInflater.inflate(R.layout.booking_item_layout, null, false);
    return new BookingViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull BookingViewHolder holder, int position) {
    BookingData data = bookings.get(position);
    holder.lblDate.setText(data.getDate());
    holder.lblLocation.setText(data.getLocation());
    holder.lblRemarks.setText(data.getRemarks());
  }

  @Override
  public int getItemCount() {
    return bookings != null ? bookings.size() : 0;
  }

  static class BookingViewHolder extends RecyclerView.ViewHolder {
    TextView lblDate;
    TextView lblLocation;
    TextView lblRemarks;

    public BookingViewHolder(@NonNull @NotNull View itemView) {
      super(itemView);
      lblLocation = itemView.findViewById(R.id.lblLocation);
      lblDate = itemView.findViewById(R.id.lblDate);
      lblRemarks = itemView.findViewById(R.id.lblRemark);
    }
  }
}
