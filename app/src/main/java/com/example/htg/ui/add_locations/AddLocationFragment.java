package com.example.htg.ui.add_locations;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.htg.R;
import com.example.htg.models.LocationData;
import com.example.htg.services.DbStore;

import java.util.UUID;

public class AddLocationFragment extends Fragment {
  DbStore store = DbStore.getInstance();
  private AddLocationViewModel addLocationViewModel;

  public View onCreateView(@NonNull LayoutInflater inflater,
                           ViewGroup container, Bundle savedInstanceState) {
    addLocationViewModel = new ViewModelProvider(this).get(AddLocationViewModel.class);
    View root = inflater.inflate(R.layout.fragment_add_location, container, false);
//    final TextView textView = root.findViewById(R.id.text_favourite);
    initViews(root);
    addLocationViewModel.get().observe(getViewLifecycleOwner(), data -> {
//      textView.setText(s);
    });
    return root;
  }

  void initViews(View root) {
    EditText txtTitle = root.findViewById(R.id.txtLocation);
    EditText txtAddress = root.findViewById(R.id.txtAddress);
    EditText txtCoordinate = root.findViewById(R.id.txtCoordinates);
    EditText txtImage = root.findViewById(R.id.txtImageURL);
    Button saveBtn = root.findViewById(R.id.btnSave);
    saveBtn.setOnClickListener(v -> {
      LocationData locationData = new LocationData();
      locationData.setLocationName(txtTitle.getText().toString());
      locationData.setAddress(txtAddress.getText().toString());
      locationData.setCoordinates(txtCoordinate.getText().toString());
      locationData.setImageUrl(txtImage.getText().toString());
      if(!locationData.valid()){
        Toast.makeText(getContext(),"Please make sure all value is valid",Toast.LENGTH_SHORT).show();
        return;
      }
      store.getStore("locations").child(UUID.randomUUID().toString())
          .setValue(locationData);
      getParentFragmentManager().popBackStack();
    });
  }
}