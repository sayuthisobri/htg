package com.example.htg.ui.add_locations;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.htg.models.LocationData;

public class AddLocationViewModel extends ViewModel {

  private final MutableLiveData<LocationData> data;

  public AddLocationViewModel() {
    data = new MutableLiveData<>();
    data.setValue(new LocationData());
  }

  public LiveData<LocationData> get() {
    return data;
  }
}