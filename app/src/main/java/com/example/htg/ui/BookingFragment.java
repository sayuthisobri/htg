package com.example.htg.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.htg.BookingActivity;
import com.example.htg.R;
import com.example.htg.models.BookingData;
import com.example.htg.services.DbStore;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textview.MaterialTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingFragment extends Fragment {
  View root;
  MaterialTextView txtMonth;
  DbStore store = DbStore.getInstance();

  public BookingFragment() {
  }

  public static BookingFragment newInstance(String param1, String param2) {
    BookingFragment fragment = new BookingFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    root = inflater.inflate(R.layout.fragment_booking, container, false);
    txtMonth = root.findViewById(R.id.month_label);
    final RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
    LinearLayoutManager layout = new LinearLayoutManager(getContext());
    recyclerView.setLayoutManager(layout);
    recyclerView.setHasFixedSize(true);
    BookingAdapter adapter = new BookingAdapter(getContext(), new ArrayList<>());
    recyclerView.setAdapter(adapter);
    CompactCalendarView calendarView = root.findViewById(R.id.calendar_view);
    Date firstDayOfCurrentMonth = calendarView.getFirstDayOfCurrentMonth();
    updateMonthTitleFromDate(firstDayOfCurrentMonth);
    store.getStore("booking").addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
        if (snapshot.exists()) {
          adapter.clear();
          snapshot.getChildren().forEach(s -> {
            adapter.add(s.getValue(BookingData.class));
          });
          adapter.notifyDataSetChanged();
        }
      }

      @Override
      public void onCancelled(@NonNull @NotNull DatabaseError error) {

      }
    });
    calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
      @Override
      public void onDayClick(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.ENGLISH);
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//        Toast.makeText(getContext(), "Click on :" + dateFormat.format(date), Toast.LENGTH_SHORT).show();
        AlertDialog alertDialog = new MaterialAlertDialogBuilder(getContext())
            .setMessage("Are you sure to book on " + dateFormat.format(date))
            .setNegativeButton("No, Thanks", (dialog, which) -> {
            })
            .setPositiveButton("Yes, please", (dialog, which) -> {
//              Snackbar.make(root, "Processing..", Snackbar.LENGTH_SHORT)
//                  .show();
              Intent newBooking = new Intent(getActivity(), BookingActivity.class);
              newBooking.putExtra("date", dateFormat1.format(date));
              getActivity().startActivity(newBooking);
            })
            .create();
        alertDialog.show();
      }

      @Override
      public void onMonthScroll(Date date) {
        getActivity().runOnUiThread(() -> {
          updateMonthTitleFromDate(date);
        });
//        updateMonthTitleFromDate(firstDayOfCurrentMonth);
      }
    });
    return root;
  }

  private void updateMonthTitleFromDate(Date date) {
    SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM, yyyy", Locale.ENGLISH);
    String monthName = monthFormat.format(date);
//    Log.d(BookingFragment.class.getCanonicalName(), "Month was scrolled to: " + date + ": " + monthName);
    txtMonth.setText(monthName);
  }
}