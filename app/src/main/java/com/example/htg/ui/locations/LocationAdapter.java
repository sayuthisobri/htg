package com.example.htg.ui.locations;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.htg.MapActivity;
import com.example.htg.R;
import com.example.htg.models.LocationData;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> {
  private final Context context;
  private final List<LocationData> locations;

  public LocationAdapter(Context context, List<LocationData> locations) {
    this.context = context;
    this.locations = locations;
  }

  public void clear() {
    this.locations.clear();
  }

  public void add(LocationData data) {
    this.locations.add(data);
  }

  @NonNull
  @NotNull
  @Override
  public LocationViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(context);
    View view = layoutInflater.inflate(R.layout.location_item_layout, null, false);
    return new LocationViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull LocationViewHolder holder, int position) {
    LocationData data = locations.get(position);
    holder.lblTitle.setText(data.getLocationName());
    holder.lblAddress.setText(data.getAddress());
    Glide.with(context).load(data.getImageUrl())
        .centerCrop()
        .into(holder.imageView);
    holder.btn.setOnClickListener(v -> {
      Toast.makeText(context, String.format("Click item %s", data.getLocationName()), Toast.LENGTH_SHORT).show();
      Bundle bundle = new Bundle();
      bundle.putString("coordinates", data.getCoordinates());
      Intent intent = new Intent(context.getApplicationContext(), MapActivity.class);
      intent.putExtra("coordinates", data.getCoordinates());
      context.startActivity(intent, bundle);
    });
  }

  @Override
  public int getItemCount() {
    return locations != null ? locations.size() : 0;
  }

  static class LocationViewHolder extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView lblTitle;
    TextView lblAddress;
    Button btn;

    public LocationViewHolder(@NonNull @NotNull View itemView) {
      super(itemView);

      imageView = itemView.findViewById(R.id.imgView);
      lblTitle = itemView.findViewById(R.id.lblLocationName);
      lblAddress = itemView.findViewById(R.id.lblLocationAddress);
      btn = itemView.findViewById(R.id.btnGetDirection);
    }
  }
}
