package com.example.htg.ui.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.htg.R;
import com.example.htg.services.DbStore;
import com.example.htg.waktusolat.PrayerTimeApi;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.hassanjamil.hqibla.CompassActivity;
import com.hassanjamil.hqibla.Constants;
import com.skydoves.powerspinner.DefaultSpinnerAdapter;
import com.skydoves.powerspinner.PowerSpinnerInterface;
import com.skydoves.powerspinner.PowerSpinnerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {

  DbStore instance = DbStore.getInstance();
  PrayerTimeApi prayerApi = instance.getPrayerApi();
  private HomeViewModel homeViewModel;
  private List<PrayerTimeApi.ZoneResponse> zones;
  private View rootView;
  private SharedPreferences userDetails;

  public View onCreateView(@NonNull LayoutInflater inflater,
                           ViewGroup container, Bundle savedInstanceState) {
    userDetails = getContext().getSharedPreferences("user_details", MODE_PRIVATE);
    homeViewModel =
        new ViewModelProvider(this).get(HomeViewModel.class);
    View root = inflater.inflate(R.layout.fragment_home, container, false);
    TextView lblWelcomeUser = root.findViewById(R.id.lblWelcomeUser);
    String welcomeText = userDetails.getString("fullname", userDetails.getString("username", "guess"));
    lblWelcomeUser.setText(String.format("Welcome, %s", welcomeText));
    final MaterialButton btnQiblat = root.findViewById(R.id.btnQiblat);
    final PowerSpinnerView dropdownZone = root.findViewById(R.id.dropdown_zone);
    final PowerSpinnerView dropdownState = root.findViewById(R.id.dropdown_state);
    btnQiblat.setOnClickListener(v -> {
      Intent intent = new Intent(HomeFragment.this.getActivity(), CompassActivity.class);
      intent.putExtra(Constants.TOOLBAR_TITLE, "Qiblat Finder");    // Toolbar Title
      intent.putExtra(Constants.TOOLBAR_BG_COLOR, "#FFFFFF");    // Toolbar Background color
      intent.putExtra(Constants.TOOLBAR_TITLE_COLOR, "#000000");  // Toolbar Title color
      intent.putExtra(Constants.COMPASS_BG_COLOR, "#FFFFFF");    // Compass background color
      intent.putExtra(Constants.ANGLE_TEXT_COLOR, "#000000");    // Angle Text color
      intent.putExtra(Constants.DRAWABLE_DIAL, R.drawable.dial);  // Your dial drawable resource
      intent.putExtra(Constants.DRAWABLE_QIBLA, R.drawable.qibla);  // Your qibla indicator drawable resource
      intent.putExtra(Constants.FOOTER_IMAGE_VISIBLE, View.VISIBLE);  // Footer World Image visibility
      intent.putExtra(Constants.LOCATION_TEXT_VISIBLE, View.VISIBLE); // Location Text visibility
      HomeFragment.this.startActivity(intent);
    });
    prayerApi.getZones(null).enqueue(new Callback<PrayerTimeApi.ZonesResponse>() {
      @Override
      public void onResponse(Call<PrayerTimeApi.ZonesResponse> call, Response<PrayerTimeApi.ZonesResponse> response) {
        PrayerTimeApi.ZonesResponse body = response.body();
        HashSet<String> states = new HashSet<>();
        zones = body.results;
        zones.forEach(z -> {
          states.add(z.negeri);
        });
        DefaultSpinnerAdapter stateAdapter = new DefaultSpinnerAdapter(dropdownState);
        List<String> stateList = Collections.unmodifiableList(new ArrayList<>(states));
        stateAdapter.setItems(stateList);
        dropdownState.setSpinnerAdapter(stateAdapter);
        setupStateListener(dropdownZone, dropdownState);
        stateAdapter.notifyDataSetChanged();
        states.stream().filter(s -> s.equalsIgnoreCase("kuala lumpur")).findFirst()
            .ifPresent(s -> dropdownState.selectItemByIndex(stateList.indexOf(s)));
//        setZoneList(zones, dropdownZone);
      }

      @Override
      public void onFailure(Call<PrayerTimeApi.ZonesResponse> call, Throwable t) {
        Log.e("err", t.getMessage());
      }
    });
    this.rootView = root;
    return root;
  }

  private void setupStateListener(PowerSpinnerView dropdownZone, PowerSpinnerView dropdownState) {
    dropdownState.setOnSpinnerItemSelectedListener((i, o, i1, v) -> {
      Log.i("state change", i + "");
      if (zones != null && v instanceof String) {
        setZoneList((zones).stream().filter(zone -> {
          if (zone != null) {
            return zone.negeri.equals(v);
          }
          return false;
        }).collect(Collectors.toList()), dropdownZone);
      }
    });
  }

  private void setZoneList(List<PrayerTimeApi.ZoneResponse> zones, PowerSpinnerView dropdownZone) {
    PowerSpinnerInterface<CharSequence> adapter = dropdownZone.getSpinnerAdapter();
//    if(adapter==null)adapter = new DefaultSpinnerAdapter(dropdownZone);
    Log.d("zones total", zones.size() + "");
    adapter.setItems(zones.stream().map(z -> String.format("%s", z.lokasi)).collect(Collectors.toList()));
    dropdownZone.setSpinnerAdapter(adapter);
    dropdownZone.setOnSpinnerItemSelectedListener((i, o, index, t1) -> {
      Object tag = dropdownZone.getTag();
      if (tag instanceof List) {
        Object zone = ((List<?>) tag).get(index);
        if (zone instanceof PrayerTimeApi.ZoneResponse) {
          prayerApi.getToday(((PrayerTimeApi.ZoneResponse) zone).zone, "12-hour")
              .enqueue(new Callback<PrayerTimeApi.PrayerTimesResponse>() {
                @Override
                public void onResponse(Call<PrayerTimeApi.PrayerTimesResponse> call, Response<PrayerTimeApi.PrayerTimesResponse> response) {
                  updatePrayerTime(response.body());
                }

                @Override
                public void onFailure(Call<PrayerTimeApi.PrayerTimesResponse> call, Throwable t) {

                }
              });
        }
      }
    });
    dropdownZone.setTag(zones);
    if (adapter instanceof DefaultSpinnerAdapter) {
      ((DefaultSpinnerAdapter) adapter).notifyDataSetChanged();
    }
    if (zones.size() > 0) dropdownZone.selectItemByIndex(0);
  }

  private void updatePrayerTime(PrayerTimeApi.PrayerTimesResponse body) {
    final MaterialTextView txtImsak = rootView.findViewById(R.id.txtImsak);
    final MaterialTextView txtSubuh = rootView.findViewById(R.id.txtSubuh);
    final MaterialTextView txtZohor = rootView.findViewById(R.id.txtZohor);
    final MaterialTextView txtAsar = rootView.findViewById(R.id.txtAsar);
    final MaterialTextView txtMaghrib = rootView.findViewById(R.id.txtMaghrib);
    final MaterialTextView txtIsyak = rootView.findViewById(R.id.txtIsyak);
    if (body.prayer_times != null) {
      txtImsak.setText(body.prayer_times.imsak);
      txtSubuh.setText(body.prayer_times.subuh);
      txtZohor.setText(body.prayer_times.zohor);
      txtAsar.setText(body.prayer_times.asar);
      txtMaghrib.setText(body.prayer_times.maghrib);
      txtIsyak.setText(body.prayer_times.isyak);
    }
  }
}