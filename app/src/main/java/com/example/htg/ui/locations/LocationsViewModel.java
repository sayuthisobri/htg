package com.example.htg.ui.locations;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LocationsViewModel extends ViewModel {

  private MutableLiveData<String> mText;

  public LocationsViewModel() {
    mText = new MutableLiveData<>();
    mText.setValue("This is planning fragment");
  }

  public LiveData<String> get() {
    return mText;
  }
}