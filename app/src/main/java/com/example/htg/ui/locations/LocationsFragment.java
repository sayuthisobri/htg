package com.example.htg.ui.locations;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.htg.R;
import com.example.htg.models.LocationData;
import com.example.htg.services.DbStore;

import java.util.ArrayList;

public class LocationsFragment extends Fragment {

  private LocationsViewModel locationsViewModel;
  private DbStore store = DbStore.getInstance();

  public View onCreateView(@NonNull LayoutInflater inflater,
                           ViewGroup container, Bundle savedInstanceState) {
//    locationsViewModel =
//        new ViewModelProvider(this).get(LocationsViewModel.class);
    View root = inflater.inflate(R.layout.fragment_locations, container, false);
    final RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    LocationAdapter adapter = new LocationAdapter(getContext(), new ArrayList<>());
    recyclerView.setAdapter(adapter);
    store.getStore("locations").get().addOnCompleteListener(task -> {
      if (task.isSuccessful() && task.getResult() != null) {
        adapter.clear();
        if (task.getResult().exists()) {
          task.getResult().getChildren().forEach(dataSnapshot -> {
            adapter.add(dataSnapshot.getValue(LocationData.class));
          });
          adapter.notifyDataSetChanged();
        }
      }
    });
    return root;
  }
}