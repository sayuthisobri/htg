package com.example.htg.models;

public class LocationData {
  private String locationName;
  private String address;
  private String coordinates;
  private String imageUrl;
  private String type;

  public String getLocationName() {
    return locationName;
  }

  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(String coordinates) {
    this.coordinates = coordinates;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  private boolean notEmpty(Object o) {
    if (o == null) return false;
    if (o instanceof String) {
      return !((String) o).trim().isEmpty();
    }
    return false;
  }

  public boolean valid() {
    return notEmpty(locationName)
        && notEmpty(address)
        && notEmpty(coordinates)
        && notEmpty(imageUrl)
        && coordinates.contains(",");
  }
}
