package com.example.htg.models;

public class User {
    public String username;
    public String fullname;
    public String email;
    public String password;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String fullname, String email) {
        this.username = username;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
    }

}
