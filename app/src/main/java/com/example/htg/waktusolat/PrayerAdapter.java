package com.example.htg.waktusolat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.example.htg.R;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class PrayerAdapter extends BaseAdapter {
  LayoutInflater inflater;
  Context context;
  private List<PrayerTime> prayerTimeList;
  public PrayerAdapter(Context context, List<PrayerTime> prayerTimeList) {
    this.context = context;
    inflater = LayoutInflater.from(context);
    this.prayerTimeList = prayerTimeList;
  }

  public List<PrayerTime> getPrayerTimeList() {
    return prayerTimeList;
  }

  @Override
  public int getCount() {
    return prayerTimeList.size();
  }

  @Override
  public PrayerTime getItem(int position) {
    return prayerTimeList.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    MaterialTextView label;
    MaterialTextView value;
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.list_waktu_solat, null);
    }
    label = convertView.findViewById(R.id.lblWaktuLabel);
    value = convertView.findViewById(R.id.lblWaktuValue);
    PrayerTime item = getItem(position);
    label.setText(item.label);
    value.setText(item.time);
    return convertView;
  }

  public static class PrayerTime {
    public String label;
    public String time;
    public PrayerTime(String label, String time) {
      this.label = label;
      this.time = time;
    }
  }
}
