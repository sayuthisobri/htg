package com.example.htg.waktusolat;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface PrayerTimeApi {
  @GET("/zone/zones.json")
  Call<ZonesResponse> getZones(@Query("state") String state);

  @GET("/zone/states.json")
  Call<PrayerTimeApi.ZonesResponse> getStates(@Query("state") String state);

  @GET("/times/today.json")
  Call<PrayerTimeApi.PrayerTimesResponse> getToday(@Query("zone") String zone, @Query("format") String format);

  class ZonesResponse {
    public List<String> states;
    public List<ZoneResponse> results;
  }

  class ZoneResponse {
    public String zone;
    public String negeri;
    public String lokasi;
    public String lat;
    public String lng;
  }

  class PrayerTimesResponse {
    public String zone;
    public String start;
    public String end;
    public List<String> locations;
    public PrayerTimes prayer_times;

    public static class PrayerTimes {
      public String date;
      public String datestamp;
      public String imsak;
      public String subuh;
      public String syuruk;
      public String zohor;
      public String asar;
      public String maghrib;
      public String isyak;
    }
  }
}
