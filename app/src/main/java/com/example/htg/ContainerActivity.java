package com.example.htg;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class ContainerActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_container);
    BottomNavigationView navView = findViewById(R.id.nav_view);
    // Passing each menu ID as a set of Ids because each
    // menu should be considered as top level destinations.
//    AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//        R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
//        .build();
    NavController navController = getNavController();
//    NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
    NavigationUI.setupWithNavController(navView, navController);
  }

  @NonNull
  private NavController getNavController() {
    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
    if (!(fragment instanceof NavHostFragment)) {
      throw new IllegalStateException("Activity " + this
          + " does not have a NavHostFragment");
    }
    return ((NavHostFragment) fragment).getNavController();
  }

}