package com.example.htg;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.example.htg.models.User;
import com.example.htg.services.DbStore;
import com.google.android.material.navigation.NavigationView;

public class LoginActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

  private final DbStore store = DbStore.getInstance();
  SharedPreferences userDetails;
  //  private final BottomNavigationView.OnNavigationItemReselectedListener navListener =
//      item -> {
//        Log.d("click", item.getTitle().toString());
//        Fragment selectedFragment = null;
//        int itemId = item.getItemId();
//        if (itemId == R.id.navigation_favourite) {
//          selectedFragment = new FavouritesFragment();
//        } else if (itemId == R.id.navigation_planning) {
//          selectedFragment = new SearchFragment();
//        } else {
//          selectedFragment = new HomeFragment();
//        }
//        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
//      };
  private EditText txtName;
  private EditText txtPassword;
  private TextView lblInfo;
  private Button btnLogin;
  private TextView lblRegister;
  private DrawerLayout drawer;
  private int counter = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    userDetails = getSharedPreferences("user_details", MODE_PRIVATE);
    setContentView(R.layout.activity_login);
// ...

    txtName = findViewById(R.id.etName);
    txtPassword = findViewById(R.id.etPassword);
    lblInfo = findViewById(R.id.tvInfo);
    btnLogin = findViewById(R.id.btnLogin);
    lblRegister = findViewById(R.id.tvRegister);
    lblInfo.setText("No of incorrect attempts: 0");

//    Re enable login button on input changes
    TextWatcher inputChangeListener = new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!btnLogin.isEnabled()) {
          btnLogin.setEnabled(true);
        }
      }

      @Override
      public void afterTextChanged(Editable s) {
      }
    };
    txtName.addTextChangedListener(inputChangeListener);
    txtPassword.addTextChangedListener(inputChangeListener);

    lblRegister.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, RegistrationActivity.class)));

    btnLogin.setOnClickListener(v -> {
      String inputName = txtName.getText().toString();
      String inputPassword = txtPassword.getText().toString();

      if (inputName.isEmpty() || inputPassword.isEmpty()) {
        Toast.makeText(LoginActivity.this, "Please Enter all the details correctly!", Toast.LENGTH_SHORT).show();
      } else {
        store.validateUser(new User(inputName, inputPassword), (success, user) -> {
          if (success != null && success) {
            counter = 0;
            Toast.makeText(LoginActivity.this, "Login was successful!", Toast.LENGTH_SHORT).show();
            SharedPreferences.Editor editor = userDetails.edit();
            editor.putString("username", user.username);
            editor.putString("fullname", user.fullname);
            editor.putString("email", user.email);
            editor.putString("password", user.password);
            editor.apply();
            Intent intent = new Intent(LoginActivity.this, ContainerActivity.class);
            startActivity(intent);
          } else {
            counter++;
            Toast.makeText(LoginActivity.this, "Incorrect credentials entered!", Toast.LENGTH_SHORT).show();
            if (counter >= 3) {
              btnLogin.setEnabled(false);
            }
          }
          lblInfo.setText("No of incorrect attempts: " + counter);
        });
      }
    });

//    BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
//    bottomNav.setVisibility(View.GONE);
//    bottomNav.setOnNavigationItemReselectedListener(navListener);
  }

    /*@Override
    protected void onCreate ( Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new MessageFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_message);
        }
    }*/

  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == R.id.nav_message) {
      getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
          new MessageFragment()).commit();
      //            case R.id.nav_chat:
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                        new chatFragment()).commit();
//                break;
//            case R.id.nav_profile:
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                        new profileFragment()).commit();
//                break;
    } else if (itemId == R.id.nav_share) {
      Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
    } else if (itemId == R.id.nav_send) {
      Toast.makeText(this, "Send", Toast.LENGTH_SHORT).show();
    }
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  public void onBackPressed() {
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

}

