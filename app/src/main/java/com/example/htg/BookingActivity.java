package com.example.htg;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import com.example.htg.services.DbStore;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.UUID;

public class BookingActivity extends AppCompatActivity {

  DbStore store = DbStore.getInstance();
  String id;
  String username;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_booking);
    String date = getIntent().getStringExtra("date");
    EditText txtName = findViewById(R.id.txtBookedBy);
    EditText txtDate = findViewById(R.id.txtBookedDate);
    txtDate.setEnabled(false);
    EditText txtLocation = findViewById(R.id.txtBookedLocation);
    EditText txtRemarks = findViewById(R.id.txtBookedRemarks);
    txtDate.setText(date);
    SharedPreferences userDetails = getSharedPreferences("user_details", MODE_PRIVATE);
    username = userDetails.getString("username", "");
    String name = userDetails.getString("fullname", username);
    txtName.setText(name);
    id = UUID.randomUUID().toString();

    MaterialButton btn = findViewById(R.id.btnBooking);
    btn.setOnClickListener(v -> {
      DatabaseReference booking = store.getStore("booking");
      HashMap<String, String> map = new HashMap<>();
      map.put("id", id);
      map.put("name", txtName.getText().toString());
      map.put("bookedBy", username);
      map.put("location", txtLocation.getText().toString());
      map.put("date", txtDate.getText().toString());
      map.put("remarks", txtRemarks.getText().toString());
      booking.child(id)
          .setValue(map);

      Snackbar.make(btn, "Add new booking", Snackbar.LENGTH_SHORT)
          .show();
      onBackPressed();
    });
  }
}