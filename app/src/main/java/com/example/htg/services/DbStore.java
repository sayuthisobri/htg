package com.example.htg.services;

import android.os.Build;
import androidx.annotation.RequiresApi;
import com.example.htg.models.LocationData;
import com.example.htg.models.User;
import com.example.htg.waktusolat.PrayerTimeApi;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import retrofit2.Retrofit;
import retrofit2.adapter.guava.GuavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class DbStore {
  static private DbStore instance;
  private DatabaseReference mDatabase;
  private List<Consumer<List<LocationData>>> locationConsumers;

  public DbStore() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
//    DatabaseReference locations = getStore("locations");
//    locations.addValueEventListener(new ValueEventListener() {
//      @Override
//      public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
//
//      }
//
//      @Override
//      public void onCancelled(@NonNull @NotNull DatabaseError error) {
//
//      }
//    });
  }

  public static DbStore getInstance() {
    if (instance == null) {
      instance = new DbStore();
    }
    return instance;
  }

  public Task<Void> saveUser(User user) {
    DatabaseReference usersRef = userStore();
    return usersRef.child(user.username).setValue(user);
  }

  private DatabaseReference userStore() {
    return getStore("users");
  }

  public DatabaseReference getStore(String ns) {
    return mDatabase.child(ns);
  }

  @RequiresApi(api = Build.VERSION_CODES.N)
  public void validateUser(User user, BiConsumer<Boolean, User> consumer) {
    DatabaseReference userStore = userStore();
    Task<DataSnapshot> dataSnapshotTask = userStore.child(user.username).get();
    dataSnapshotTask.addOnCompleteListener(task -> {
      try {
        if (task.isSuccessful()) {
          User response = task.getResult().getValue(User.class);
          if (response.password.equals(user.password)) {
            consumer.accept(true, user);
            return;
          }
        }
        throw new Exception();
      } catch (Exception e) {
        consumer.accept(false, null);
      }
    });
  }

  public PrayerTimeApi getPrayerApi() {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("http://api.azanpro.com")
        .addCallAdapterFactory(GuavaCallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build();
    return retrofit.create(PrayerTimeApi.class);
  }
}
