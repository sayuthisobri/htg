package com.example.htg;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.htg.models.User;
import com.example.htg.services.DbStore;

public class RegistrationActivity extends AppCompatActivity {
  private EditText eRegName;
  private EditText eRegPassword;
  private Button eRegister;
  private DbStore store = DbStore.getInstance();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_registration);

    eRegName = findViewById(R.id.etRegName);
    eRegPassword = findViewById(R.id.etRegPassword);
    eRegister = findViewById(R.id.btnRegister);
    EditText txtFullname = findViewById(R.id.txtFullname);
    EditText txtEmail = findViewById(R.id.txtEmail);

    eRegister.setOnClickListener(v -> {
      String username = eRegName.getText().toString();
      String password = eRegPassword.getText().toString();

      if (validate(username, password)) {
        this.store.saveUser(new User(username, password, txtFullname.getText().toString(), txtEmail.getText().toString()));
        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
        Toast.makeText(RegistrationActivity.this, "Register Successful!", Toast.LENGTH_SHORT).show();
      }

    });
  }

  private boolean validate(String username, String password) {
    if (username.isEmpty() || password.length() < 8) {
      Toast.makeText(this, "Please enter all the details, password should be at least 8 characters!", Toast.LENGTH_SHORT).show();
      return false;
    }
    return true;
  }
}

